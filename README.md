# PyRadiaUtils

Python utilities for RADIA models.

radia_mat_util.py defines magnetic material
radia_util.py defines save and load functions (can be used to export objects to Mathematica)

Gael Le Bec, ESRF, 2019

## Installation

No setup file is provided from now, but the installation is rather simple:

- Clone the repository where you want
- Navigate to your python *Lib/site-packages* folder
- Create a *radia_utils.pth* file containing the path to the repository