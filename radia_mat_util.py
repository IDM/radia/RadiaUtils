# ----------------------------------------------------------
# PyRadiaUndulators library
# radia_mat_util.py
# Useful materials for Radia models
#
# Gael Le Bec, ESRF, 2019
# ----------------------------------------------------------

import radia as rad
from math import pi
from scipy.interpolate import griddata

# -------------------------------------------
# Materials
# -------------------------------------------
# XC6 / AISI 1006 low carbon steel
# xc6 = rad.MatSatIsoFrm([2118, 1.362], [63.06, 0.2605], [17.138, 0.4917])
# AFK1 FeCo Alloy from MetalImphy (Fe : 74.2, Co: 25%, Cr: 0.3%, Mn: 0.5%)
# fe_co = rad.MatSatIsoFrm([2001., 1.704], [38.56, 0.493], [1.24, 0.152])
# AFK502 Vanadium Permendur alloy from MetalImphy (Fe : 49%, Co: 49%, V: 2%) similar to Vacoflux50 from VacuumSchmelze
# fe_co_v = rad.MatSatIsoFrm([10485., 1.788], [241.5, 0.437], [7.43, 0.115])
# Armco pure iron
# armco = rad.MatSatIsoFrm([4236., 1.385], [126.2, 0.2649], [34., 0.5])

def set_soft_mat(mat_name):
    """"
    Returns a soft magnetic material according to mat_name
    :param mat_name: material specification
            -- 'xc6': XC6 / AISI 1006 low carbon steel
            -- 'feco': FeCo Alloy
            -- 'fecov': Vanadium Permendur alloy
            -- 'armco': Armco Pure Iron
            -- '1300-100a': Isovac 1300-100A electric steel from VostAlpine
            -- 'ni': Pure nickel
    :return the material
    """
    mu_0 = 4. * pi * 1e-07
    if mat_name == 'xc6':
        mat = rad.MatSatIsoFrm([2118, 1.362], [63.06, 0.2605], [17.138, 0.4917])
    elif mat_name == 'feco':
        mat = rad.MatSatIsoFrm([2001., 1.704], [38.56, 0.493], [1.24, 0.152])
    elif mat_name == 'fecov':
        mat = rad.MatSatIsoFrm([10485., 1.788], [241.5, 0.437], [7.43, 0.115])
    elif mat_name == 'armco':
        mat = rad.MatSatIsoFrm([4236., 1.385], [126.2, 0.2649], [34., 0.5])
    elif mat_name == '1300-100a':
        h = [48, 56, 65, 74, 82, 91, 100, 108, 117, 125, 134, 143, 152, 161, 171, 182, 193, 217, 230, 243, 255, 271,
             294, 318, 344, 401, 517, 698, 983, 1559, 2557, 3930, 7500, 10000, 25000, 40000, 100000]
        j = [0.100, 0.150, 0.200, 0.250, 0.300, 0.350, 0.400, 0.450, 0.500, 0.550, 0.600, 0.650, 0.700, 0.750, 0.800,
             0.850, 0.900, 1.000, 1.050, 1.100, 1.150, 1.200, 1.250, 1.300, 1.350, 1.400, 1.450, 1.500, 1.550, 1.600,
             1.650, 1.700, 1.799, 1.847, 2.000, 2.050, 2.060]
        # Note: Points (25000, 2000) and higher fields extrapolated
        mat = rad.MatSatIsoTab([[mu_0 * h[i], 1. * j[i]] for i in range(len(h))])
    elif mat_name == 'ni':
        h = [0, 4.66354, 8.69715, 15.4198, 26.5123, 45.3358, 69.5375, 113.907, 156.932, 250, 500, 10000] 
        j = [0, 0.17756, 0.28847, 0.38594, 0.44979, 0.49683, 0.53714, 0.56733, 0.58745, 0.59568, 0.60937, 0.62]
        # Note: Points (250, ) and higher fields extrapolated
        mat = rad.MatSatIsoTab([[mu_0 * h[i], 1. * j[i]] for i in range(len(h))])
    else:
        print('Unknown material set to xc6')
        mat = rad.MatSatIsoFrm([2118, 1.362], [63.06, 0.2605], [17.138, 0.4917])
    return mat

def set_pm_mat(mat_name, br=None):
    """
    Returns a hard PM material
    :param mat_name: material type
            -- 'ndfeb': NdFeB material
            -- 'prfeb80K': Cryo PrFeB material
            -- 'sm2co17': Sm2Co17 material
            -- 'smco5': SmCo5 material
            -- 'ferrite': ferrite material
    :param br=None: remanent field
    :return the material
    """
    if mat_name == 'ndfeb':
        if br is None:
            br = 1.3
        mat = rad.MatStd('NdFeB', br)
		# ksi = [0.02, 0.06]
    elif mat_name == 'prfeb80K':
        if br is None:
            br = 1.6
        mat = rad.MatLin([0.02, 0.06], br)
    elif mat_name == 'sm2co17':
        if br is None:
            br = 1.1
        mat = rad.MatStd('Sm2Co17', br)
    elif mat_name == 'smco5':
        if br is None:
            br = 0.85
        mat = rad.MatStd('SmCo5', br)
    elif mat_name == 'ferrite':
        if br is None:
            br = 0.35
        mat = rad.MatStd('Ferrite', br)
    else:
        mat = None

    return mat

def hts_mat(temp, field_perp, rebco_thickness=0.0016, tape_width=4, tape_thickness=0.1):
    """
    Critical current and current density in a HTS tape, according to SuperPower data
    :param temp: temperature (K)
    :param field_perp: perpendicular field (T)
    :param rebco_thickness: R(E)BCO material thickness (mm)
    :param tape_width: HTS tape width (mm)
    :param tape_thickness: HTS tape thickness (mm)
    :return current (A), current_dens (A/mm^2), current_dens_eng (A/mm^2)
    """
    
    rebco_thickness0 = 0.0016 # (mm)
    tape_width0 = 4 # (mm)

    temp_field0 = [
        [4.2, 2], [4.2, 3], [4.2, 4],  # Note: the data were extrapolated at 4.2K and B < 5 T (i.e. this line)
        [4.2, 5], [4.2, 6], [4.2, 7], [4.2, 8], [4.2, 9], [4.2, 10], [4.2, 11],  [4.2, 12], [4.2, 13], [4.2, 14], [4.2, 15],
        [10, 5], [10, 6], [10, 7], [10, 8], [10, 9], [10, 10], [10, 11], [10, 12], [10, 13], [10, 14], [10, 15], 
        [20, 4], [20, 5], [20, 6], [20, 7], [20, 8], [20, 9], [20, 10], [20, 11], [20, 12], [20, 13], [20, 14], [20, 15],
        [30, 2], [30, 3], [30, 4], [30, 5], [30, 6], [30, 7], [30, 8], [30, 9], [30, 10], [30, 11], [30, 12], [30, 13], [30, 14], [30, 15],
        [40, 2], [40, 3], [40, 4], [40, 5], [40, 6], [40, 7], [40, 8], [40, 9], [40, 10],
        [50, 1], [50, 2], [50, 3], [50, 4], [50, 5], [50, 6], [50, 7], [50, 8], [50, 9], [50, 10]
    ]

    current0 = [
        1200, 1000, 830,  # Note: the data were extrapolated at 4.2K and B < 5 T (i.e. this line)
        700, 615, 545, 495, 455, 420, 390, 370, 345, 325, 305, 
        580, 510, 450, 410, 370, 340, 310, 295, 270, 255, 245,
        500, 425, 370, 320, 290, 255, 230, 215, 195, 180, 170, 155,
        555, 420, 350, 300, 255, 225, 195, 175, 160, 145, 130, 120, 110, 100,
        390, 300, 250, 210, 180, 155, 140, 120, 100,
        370, 260, 200, 165, 140, 115, 95, 80, 70, 60
    ]
    
    # Interpolate
    res = griddata(temp_field0, current0, (temp, field_perp), method='linear')

    # Current (A)
    current = res * rebco_thickness * tape_width / rebco_thickness0 / tape_width0

    # Current density (A/mm^2)
    current_dens = current / rebco_thickness / tape_width

    # Engineering current density
    current_dens_eng = current / tape_thickness / tape_width

    return current, current_dens, current_dens_eng

    